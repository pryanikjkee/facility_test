//@prepros-append slider.js
//@prepros-append cart.js

let _ = (events, target, func) => {
  events.split(' ').forEach((event) => {
    document.addEventListener(event, (e) => {
      document.querySelectorAll(target).forEach((item) => {
        let element = e.target;
        if (item == element)
          return func(e, element);
        else{
          while(element.parentElement){
            if (item == element){
              return func(e, element);
            }
            else
              element = element.parentElement;
          }
        }
      });
      return false;
    });
  });
};