class Cart {
	constructor(item) {
		this.item = item;
		this.items = [...item.children];
		this.cart = document.querySelector('.nav__cart');
		this.listContainer = this.cart.querySelector('.nav__cart--list')
		this.init();
		this.toggleCart();
		this.cartItems = [];
		this.totalValue = 0;
		this.itemsCounter= document.querySelector('.nav__top--counter');
	}
	init() {
		this.observeItems();
		this.cartCount = document.querySelector('.nav__cart--text');
		this.totalPrice = document.querySelector('.nav__cart--price');
	}
	observeItems() {
		this.items.forEach((item, i) => item.querySelector('.arrivals__item--addcart')
			.addEventListener('click', () => {
				this.appendItem(i)
			})
		)
	}
	appendItem(item) {
		if (!this.cartItems.some(x => x.id === item)) {
			this.cartItems.push({
				id: item,
				photo: this.item.children[item].querySelector('.arrivals__item--photo img').src,
				name: this.item.children[item].querySelector('.arrivals__item--name').innerHTML,
				price: this.item.children[item].querySelector('.arrivals__item--price').innerHTML,
				count: 1
			});
			this.renderItems();

		} else {
			if (this.cartItems.some(x => x.id === item)) {
				this.cartItems.filter(x => x.id === item)[0]['count'] = this.cartItems.filter(x => x.id === item)[0]['count'] + 1;
				this.renderItems();
			}
		}!this.cart.classList.contains('active') && this.cart.classList.add('active');
	}
	renderItems() {
		let cls = 'nav__cart';
		this.listContainer.innerHTML = '';
		this.totalValue = 0;
		this.cartCount.innerHTML = ` Моя корзина (${this.cartItems.length})`;
		this.itemsCounter.innerHTML = `${this.cartItems.length}`
		this.cartItems.forEach(item => {
			let container = document.createElement('div');
			container.classList.add(`${cls}--item`);
			container.dataset.id = item.id;
			container.innerHTML = ` 
            <div class="nav__cart--photo"> <img src="${item.photo}" alt=""></div>
            <div class="nav__cart--info"> 
              <div class="nav__cart--name">${item.name}</div>
              <div class="nav__cart--size">Size: XS</div>
              <div class="nav__cart--values"> 
                <div class="nav__cart--counter"> 
                  <div class="nav__cart--dec cart__count">- </div>
                  <input class="nav__cart--value" type="text" value="${item.count}">
                  <div class="nav__cart--inc cart__count">+</div>
                </div>
                <div class="nav__cart--price">${parseInt(item.price)*item.count} UAH</div>
              </div>
            </div>
			`

			this.listContainer.appendChild(container);
			this.totalValue += (parseInt(item.price)) * item.count;
		});
		this.totalPrice.innerHTML = `${this.totalValue} UAH`;
		this.observeCounts();
	}
	observeCounts() {
		document.querySelectorAll('.cart__count').forEach((item, i) => item.onclick = (e) => this.count(i, e))
	}
	count(i, e) {
		let currentItem = e.target.parentNode.parentNode.parentNode.parentNode.dataset.id;

		(i % 2) ? this.cartItems.filter(x => x.id == currentItem)[0]['count'] = this.cartItems.filter(x => x.id == currentItem)[0]['count'] + 1:
			this.cartItems.filter(x => x.id == currentItem)[0]['count'] > 1 ?
			this.cartItems.filter(x => x.id == currentItem)[0]['count'] = this.cartItems.filter(x => x.id == currentItem)[0]['count'] - 1 : '';
		this.renderItems();
	}
	toggleCart() {
		document.querySelector('.nav__top--cart').addEventListener('click', () => {
			this.cart.classList.add('active')
		});
		document.querySelector('.nav__cart--close').addEventListener('click', () => {
			this.cart.classList.remove('active')
		})
	}

}

document.addEventListener('DOMContentLoaded', () => {
	let item = document.querySelector('.arrivals__container ');
	if (item) {
		new Cart(item)
	}
})