
class Slider{
    constructor(item){
      this.slider = item;
      this.slides = item.querySelector('.main__slider--container  ');
      this.arrows = [...item.querySelectorAll('.main__slider--nav')];
      this.blocks = [...this.slides.children];
      this.length = this.blocks.length;
  
      this.arrows.forEach((item, i) => item.onclick = () => this.click(i));
      this.slides.addEventListener('touchstart', this.touchstart.bind(this));
      this.slides.addEventListener('touchmove', this.touchmove.bind(this));
      document.addEventListener('touchend', this.touchend.bind(this));
      window.addEventListener('resize', this.init.bind(this));
  
      this.init();
    }
  
    init(){
      this.current = 0;
      this.blockPerSlide = parseInt(this.slides.offsetWidth / this.blocks[0].offsetWidth);
      this.slidesCount = parseInt(this.length / this.blockPerSlide);
      this.slides.style.transform = 'translateX(0%)';
    }
  
    click(dir){
      
      this.current = dir ? this.current + 1 : this.current - 1;
      this.current = this.current > this.slidesCount - 1
        ? 0 
        : this.current < 0 
        ? this.slidesCount - 1
        : this.current;
  
      this.move();
    }
  
    touchstart(e){
      this.start = e.targetTouches[0].pageX;
    }
  
    touchmove(e){
      if (this.start){
        let coords = e.changedTouches[0].pageX;
        let offset = parseFloat(getComputedStyle(this.blocks[0]).marginRight) * this.current;
  
        this.diff = this.start >= coords ? -(this.start - coords) : this.start < coords ? coords - this.start : false;
        this.slides.style.transition = '0s';
        this.slides.style.transform = `translateX(calc(-${100 * this.current}% - ${offset - this.diff}px))`;
      }
    }
  
    touchend(e){
      if (this.start){
        this.start = null;
        this.end = e.changedTouches[0].pageX;
  
        this.slides.style.transition = null;
  
        if (this.diff > 0 && this.diff >= this.blocks[0].offsetWidth / 2) this.click(0);
        else if (this.diff < 0 && Math.abs(this.diff) >= this.blocks[0].offsetWidth / 2) this.click(1);
        else this.move();  
      }
    }
  
    move(){
      let offset = parseFloat(getComputedStyle(this.blocks[0]).marginRight) * this.current;
      this.slides.style.transform = `translateX(calc(-${100 * this.current}% - ${offset}px))`;
    }
  }
  
  document.addEventListener('DOMContentLoaded', () => {
    let sliders = [...document.querySelectorAll('.main__slider ')];
    sliders.forEach(item => new Slider(item));
  });